# Mono-s results scripts
This script parses the pre and post-fit CSV files from WSMaker to create a combined plot of all
signal and control regions in the mono-s(VV) hadronic analysis.

The script formats the plot to adhere to official ATLAS style formatting, so it relies on the
atlas-mpl-style pymodule that provides a Matplotlib style replicating the one used by ATLAS.

The script solely relies on pandas, numpy, and Matplotlib. ROOT is not required.


```
usage: fitPlot.py [-h] [-post POSTFITFILE] [-pre PREFITFILE] [--blind] plotType

positional arguments:
  plotType              plotType=`pre` or `post` for pre-fit or post-fit plot

optional arguments:
  -h, --help            show this help message and exit
  -post POSTFITFILE, --postFitFile POSTFITFILE
  -pre PREFITFILE, --preFitFile PREFITFILE
  --blind
  ```
