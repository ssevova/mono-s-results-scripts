#!/usr/bin/env python
import numpy as np
import pandas as pd
import os
import matplotlib.ticker as tick
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import atlas_mpl_style as ampl
from argparse import ArgumentParser
# Set ATLAS style
ampl.use_atlas_style()
ampl.set_color_cycle(pal='ATLAS')
plt.rcParams["font.size"] = 12

"""This script parses the pre and post-fit CSV files from WSMaker to create a combined plot of all
signal and control regions in the mono-s(VV) hadronic analysis.

The script formats the plot to adhere to official ATLAS style formatting, so it relies on the
atlas-mpl-style pymodule that provides a Matplotlib style replicating the one used by ATLAS.

The script solely relies on pandas, numpy, and Matplotlib. ROOT is not required.

File name: fitPlot.py
Author: Stanislava Sevova
Date created: 05/04/2020
Date last modified: 07/24/2020
"""
def getArgumentParser():
    parser = ArgumentParser()
    parser.add_argument("plotType", help="plotType=`pre` or `post` for pre-fit or post-fit plot")
    parser.add_argument("-post", "--postFitFile", default="SWW0016-unblind/Yields_GlobalFit_conditionnal_mu0.csv")
    parser.add_argument("-pre", "--preFitFile", default="SWW0016-unblind/Yields_Prefit.csv")
    parser.add_argument("--blind", action="store_true")
    parser.add_argument("--CRonly", action="store_true")
    parser.add_argument("--verbose", action="store_true")
    parser.add_argument("--mergeVVandVH", action="store_true")
    parser.add_argument("--doYAML", action="store_true", help="Write an output YAML file")
    return parser


def getBinLabelsOld(dataframe):
    """
    Returns the dataframe
    with labelled bins
    """
    # Label the bins
    dataframe['label'] = [-999] * len(dataframe.index)

    
    # SR merged 500+
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J1')
                  & dataframe['region'].str.contains('BMin500'), 'label'] = 1
    # SR merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J1')
                  & dataframe['region'].str.contains('BMin300'), 'label'] = 2
    # SR inter 500
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J2')
                  & dataframe['region'].str.contains('BMin500'), 'label'] = 3
    # SR inter 300-500
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J2')
                  & dataframe['region'].str.contains('BMin300'), 'label'] = 4
    # SR inter 200-300
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J2')
                  & dataframe['region'].str.contains('BMin200'), 'label'] = 5

    # CR1 merged 500+
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 6
    # CR1 merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 7
    # CR1 inter 500
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 8
    # CR1 inter 300-500
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 9
    # CR1 inter 200-300
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin200'), 'label'] = 10

    # CR2 merged 500+
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 11
    # CR2 merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 12
    # CR2 inter 500
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 13
    # CR2 inter 300-500
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 14
    # CR2 inter 200-300
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin200'), 'label'] = 15
    return dataframe

def getBinLabels(dataframe):
    """
    Returns the dataframe
    with labelled bins
    """
    # Label the bins
    dataframe['label'] = [-999] * len(dataframe.index)

    
    # SR merged 500+
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J1')
                  & dataframe['region'].str.contains('BMin500'), 'label'] = 9
    # SR merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J1')
                  & dataframe['region'].str.contains('BMin300'), 'label'] = 8
    # SR inter 500
    dataframe.loc[dataframe['region'].str.contains('D0lep')
                  & dataframe['region'].str.contains('J2')
                  & dataframe['region'].str.contains('BMin200'), 'label'] = 7

    # CR1 merged 500+
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 3
    # CR1 merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 2
    # CR1 inter 500
    dataframe.loc[dataframe['region'].str.contains('D1lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin200'), 'label'] = 1

    # CR2 merged 500+
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin500'), 'label'] = 6
    # CR2 merged 300-500
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J1')
                 & dataframe['region'].str.contains('BMin300'), 'label'] = 5
    # CR2 inter 500
    dataframe.loc[dataframe['region'].str.contains('D2lep')
                 & dataframe['region'].str.contains('J2')
                 & dataframe['region'].str.contains('BMin200'), 'label'] = 4
    return dataframe


def formatProcess(dataframe):
    """
    Adds in zeros if a
    process is missing
    from a region bin
    """
    for i in range(1,10):
        if not (i in dataframe['label'].tolist()):
            line = pd.DataFrame({"label": i, "yield": 0.0, "uncertainty": 0.0},index=[np.random.randint(100)])
            dataframe = dataframe.append(line,sort=False)

    dataframe = dataframe.sort_values('label')
    dataframe = dataframe.reset_index(drop=True)

    return dataframe

#=============================================================================#    
def main():
    args = getArgumentParser().parse_args()


    post_fit = pd.read_csv(args.postFitFile)
    pre_fit = pd.read_csv(args.preFitFile)

    # Label the bins
    post_fit = getBinLabels(post_fit)
    pre_fit  = getBinLabels(pre_fit)
        
    # Make process dataframes
    if args.plotType == 'post':
        zjets   = post_fit[post_fit['sample'].str.contains('zjets')]
        wjets   = post_fit[post_fit['sample'].str.contains('wjets')]
        vhbb    = post_fit[post_fit['sample'].str.contains('VHbb')]
        diboson = post_fit[post_fit['sample'].str.contains('diboson')]
        data    = post_fit[post_fit['sample'].str.contains('data')]
        stop    = post_fit[post_fit['sample'].str.contains('stop')]
        ttbar   = post_fit[post_fit['sample'].str.contains('ttbar')]
        totbkg  = post_fit[post_fit['sample'].str.contains('Bkg')]
        prefit  = pre_fit[pre_fit['sample'].str.contains('Bkg')]
    elif args.plotType == 'pre':
        zjets   = pre_fit[pre_fit['sample'].str.contains('zjets')]
        wjets   = pre_fit[pre_fit['sample'].str.contains('wjets')]
        vhbb    = pre_fit[pre_fit['sample'].str.contains('VHbb')]
        diboson = pre_fit[pre_fit['sample'].str.contains('diboson')]
        data    = pre_fit[pre_fit['sample'].str.contains('data')]
        stop    = pre_fit[pre_fit['sample'].str.contains('stop')]
        ttbar   = pre_fit[pre_fit['sample'].str.contains('ttbar')]
        totbkg  = pre_fit[pre_fit['sample'].str.contains('Bkg')]


    # Add zeros if no yields in a bin
    # and drop original df indices 
    zjets   = formatProcess(zjets)
    wjets   = formatProcess(wjets)
    vhbb    = formatProcess(vhbb)
    diboson = formatProcess(diboson)
    data    = formatProcess(data)
    stop    = formatProcess(stop)
    ttbar   = formatProcess(ttbar)
    totbkg  = formatProcess(totbkg)
    if args.plotType == 'post':
        prefit  = formatProcess(prefit)
    if args.mergeVVandVH:
        diboson.append(vhbb)

    if args.blind:
        data   = data[3:]
        totbkg = totbkg[3:]
        prefit = prefit[3:]
        zjets  = zjets[3:]
        wjets  = wjets[3:]
        vhbb   = vhbb[3:]
        diboson = diboson[3:]
        stop   = stop[3:]
        ttbar  = ttbar[3:]

    if args.verbose:
        print(zjets)
        print(wjets)
        print(diboson)
        print(ttbar)
        print(stop)
        print(vhbb)
        print(data)
        print(totbkg)
        if args.plotType == 'post':
            print(prefit)
        
    # Compute some np arrays for plotting later
    dataOverMC = np.divide(np.array(data['yield']),np.array(totbkg['yield']))
    ratioUncMC = np.divide(np.array(2*totbkg['uncertainty']), np.array(totbkg['yield']))
    totbkg['relUnc'] = (2*totbkg['uncertainty'])/totbkg['yield']
    totbkg['totUnc'] = np.sqrt(totbkg['uncertainty']**2 + data['yield'])
    
    if args.verbose: print(totbkg)
    ratioMinusHalfUncMC = 1 - 0.5*np.array(totbkg['relUnc'])

    if args.plotType == 'post':
        preOverPost = np.divide(np.array(prefit['yield']),np.array(totbkg['yield']))
        numer = np.array(prefit['yield'])
        denom = np.array(totbkg['yield'])
        numererr = np.array(prefit['uncertainty'])
        denomerr = np.array(totbkg['uncertainty'])
        errPreOverPost = np.sqrt(numererr*numererr/denom/denom + preOverPost*preOverPost*denomerr*denomerr/denom/denom)

    errDataOverMC = np.divide(1,np.sqrt(np.array(data['yield']))) 

    #Declare the region labels
    regionnameOld = ['SR merged 500+', 'SR merged 300-500',
                  'SR intermediate 500+', 'SR intermediate 300-500', 'SR intermediate 200-300',
                  'CR1 merged 500+', 'CR1 merged 300-500',
                  'CR1 intermediate 500+', 'CR1 intermediate 300-500', 'CR1 intermediate 200-300',
                  'CR2 merged 500+', 'CR2 merged 300-500',
                  'CR2 intermediate 500+', 'CR2 intermediate 300-500', 'CR2 intermediate 200-300'
    ]

    regionname = ['Intermediate',
                  'Merged, $[300,500]$',
                  'Merged, $[500,\infty)$',
                  'Intermediate',
                  'Merged, $[300,500]$',
                  'Merged, $[500,\infty)$',
                  'Intermediate',
                  'Merged, $[300,500]$',
                  'Merged, $[500,\infty)$']


    if args.verbose:
        for reg,unc in zip(regionnameOld,totbkg['totUnc']):
            print("Reg: ", reg, ", uncertainty: ", unc)

    if args.doYAML:

        outYmlName="SRandCR_fit.yaml"
        if args.CRonly: outYmlName="CRonly_fit.yaml"
        
        with open(outYmlName,'w') as outyml:
            outyml.write(
'''dependent_variables:
- header: {name: Events / region}
  qualifiers:
  - {name: ., value: DATA}
  - {name: cmenergies, units: GeV, value: 13000}
  - {name: reaction, value: P P --> DARKHIGGS DM DM}
  - {name: region, value:SR CRs}
  values:''')
            for data in np.array(data['yield']):
                outyml.write(
'''
  - errors:
    - {{symerror: {:.2f}}}
    value: {}'''.format(np.sqrt(data),data))
            outyml.write(
'''
- header: {name: Events / region}
  qualifiers:
  - {name: ., value: BACKGROUND}
  - {name: cmenergies, units: GeV, value: 13000}
  - {name: reaction, value: P P --> DARKHIGGS DM DM}
  - {name: region, value:SR CRs}
  values:''')
            for bkg,bkgUnc in zip(np.array(totbkg['yield']),np.array(totbkg['uncertainty'])):
                outyml.write(
'''
  - errors:
    - {{symerror: {:.2f}}}
    value: {:.1f}'''.format(bkgUnc,bkg))
            outyml.write(
'''
independent_variables:
- header: {name ''}
  values:
''')
            for region in regionname:
                if region == 'Intermediate': outyml.write("  - {high: inf, low: 200.0}\n")
                elif region == 'Merged, $[300,500]$': outyml.write("  - {high: 500.0, low: 300.0}\n")
                elif region == 'Merged, $[500,\infty)$': outyml.write("  - {high: inf, low: 500.0}\n")
                
    indOld = range(1,16)
    ind = range(1,10)
    if args.blind:
        indOld = indOld[5:]
        ind = ind[3:]
        regionnameOld = regionnameOld[5:]
        regionname = regionname[3:]
    xerr = [0.5] * len(ind)
    binwidth=1.0

            

    # Copied from https://github.com/beojan/atlas-mpl/blob/master/atlas_mpl_style/__init__.py#L298
    # but had to modify the dpi
    fig = plt.figure(figsize=(9, 6),dpi=100)
    gs = gridspec.GridSpec(7, 1, hspace=0.0, wspace=0.0)
    ax1 = fig.add_subplot(gs[0:5])
    ax1.tick_params(labelbottom=False)
    ax2 = fig.add_subplot(gs[5:7], sharex=ax1)
    ax2.yaxis.set_major_locator(tick.LinearLocator(numticks=5))
    ax2.xaxis.set_major_locator(tick.MaxNLocator(symmetric=True, prune='both', min_n_ticks=5, nbins=4))
    ax2.autoscale(axis="x", tight=True)

    # Add the bar plots for every process
    # and dummy plot for data legend entry
    h=ax1.bar(ind, np.array(diboson['yield']), width=binwidth, label=r'$Diboson + VH$', color='#FC4F06', edgecolor='black', zorder=-5)

    if not args.mergeVVandVH:
        ax1.bar(ind, np.array(vhbb['yield']), width=binwidth, label=r'$SM~Vh$', color='#BF44FF', edgecolor='black',zorder=-5)

    ax1.bar(ind, np.array(ttbar['yield'])+np.array(stop['yield']), width=binwidth, label=r'$t\displaystyle\bar{t} + single~top$', color='#23FF07', edgecolor='black',
            bottom=np.array(diboson['yield']),zorder=-5)
    
    ax1.bar(ind, np.array(wjets['yield']), width=binwidth, label=r'$W+jets$', color='#21FFFF', edgecolor='black',
            bottom=np.array(diboson['yield'])+np.array(ttbar['yield'])+np.array(stop['yield']),zorder=-5)

    ax1.bar(ind, np.array(zjets['yield']), width=binwidth, label=r'$Z+jets$', color='#FFFF09', edgecolor='black',
            bottom=np.array(wjets['yield'])+np.array(diboson['yield'])+np.array(ttbar['yield'])+np.array(stop['yield']),zorder=-5)

    ax1.bar(ind, np.array(totbkg['uncertainty']), width=binwidth, label='Background Uncertainty', color='None', edgecolor='None', hatch='\\\\\\\\\\',
            bottom=np.array(totbkg['yield'])-0.5*np.array(totbkg['uncertainty']),zorder=-5)
    
    ax1.plot([],[],color='black',marker='o',ls='-',label='Data')
    ax1.plot(ind,np.array(data['yield']),color="black",marker="o", ls="",zorder=1)
    #ax1.bar(ind, np.array(data['yield']),width=binwidth,color='None',edgecolor='None',yerr=np.sqrt(np.array(data['yield'])),xerr=xerr,zorder=-5)
    ax1.axvline(x=3.5,ymax=0.7,color='black',ls='--')
    ax1.axvline(x=6.5,ymax=0.7,color='black',ls='--')
    #ax1.text(1.2,15e4,'Signal Region')
    #ax1.text(4.1,15e4,'$1\mu$ Control Region')
    #ax1.text(7.1,15e4,'$2\ell$ Control Region')

    #ax1.text(1.4,9.5e4,'Signal Region')
    #ax1.text(4.3,9.5e4,'$1\mu$ Control Region')
    #ax1.text(7.3,9.5e4,'$2\ell$ Control Region')

    ax1.text(1.4,9.5e4,'$1\mu$ Control Region')
    ax1.text(4.3,9.5e4,'$2\ell$ Control Region')
    ax1.text(7.4,9.5e4,'Signal Region')
    
    ax1.xaxis.set_ticklabels([])
    ampl.set_ylabel("Events / region",ax=ax1)
    ax1.set_yscale("log")
    ax1.yaxis.get_ticklabels()[1].set_visible(False)
    ax1.set_ylim(10e-1,10e7)
    ax1.set_xlim(0.5,9.5)
    handles, labels = ax1.get_legend_handles_labels()
    ax1.legend((handles[0],handles[4],handles[3], handles[2],handles[1],handles[5]),
               (labels[0],labels[4],labels[3], labels[2],labels[1],labels[5]),
               loc='upper left', ncol=2, frameon=False)
    ampl.draw_atlas_label(x=0.68,y=0.95,ax=ax1,status='prelim',energy='13 TeV',lumi=139.0)    

    if args.plotType == 'pre':
        ax1.text(6.6,9e5, 'Pre-fit')
    elif args.plotType == 'post':
        if args.CRonly:
            ax1.text(6.6,9e5,'CR only fit')
        else:
            ax1.text(6.6,9e5,'SR and CR fit')
        
    # Make the ratio panel
    ax2.set_ylim(0.4,1.6)
    ax2.set_xlim(0.5,9.5)

    ax2.axhline(y=1, color='black',linewidth=0.8,alpha=0.5)
    ax2.bar(ind, ratioUncMC, width=binwidth,
            label='Stat. + syst.',
            color='None', edgecolor='None', hatch='\\\\\\\\\\',
            bottom=ratioMinusHalfUncMC, zorder=0)
    ax2.bar(ind, dataOverMC, width=binwidth, color='None', edgecolor='None', ecolor='black',yerr=errDataOverMC,xerr=0,zorder=2)
    ax2.plot(ind, dataOverMC, color='black', marker='o',ls='',zorder=1)
    if args.plotType == 'post':
        ax2.bar(ind, preOverPost, width=binwidth, color='None', edgecolor='None', ecolor='red',yerr=errPreOverPost,xerr=xerr,zorder=-1)
        ax2.plot([],[], color='red', ls='-',label='Pre-fit / pred.')

    ax2.axvline(x=3.5,color='black',ls='--')
    ax2.axvline(x=6.5,color='black',ls='--')

    ampl.set_ylabel("Data / pred.",ax=ax2)
    
    #ampl.set_xlabel("Region",ax=ax2, labelpad=-20)
    xticks_pos = [1.0*patch.get_width() + patch.get_xy()[0] for patch in h]
    plt.xticks(xticks_pos, regionname, ha='right', rotation=22)
    #ax2.legend(loc='lower left' if args.plotType == 'post' else 'upper right', frameon=False)
    if args.plotType == 'post':
        ax3 = ax2.twinx()
        ax3.set_ylim(0.4,1.6)
        ax3.yaxis.set_major_locator(tick.LinearLocator(numticks=5))
        ax3.tick_params(axis='y', labelcolor='red')
        ax3.spines['right'].set_color('red')
        ampl.set_ylabel("Pre-fit / pred.", color='red',ax=ax3)    
    plt.subplots_adjust(bottom=0.22,top=0.95)
    plt.savefig("prelim_SR_CR_%sfit_plot%s.pdf"%(args.plotType,
                                          "-CRfit" if args.CRonly else ""),
                format="pdf")
    plt.savefig("prelim_SR_CR_%sfit_plot%s.png"%(args.plotType,
                                          "-CRfit" if args.CRonly else ""),
                format="png")
    
if __name__ == '__main__':
    main()
